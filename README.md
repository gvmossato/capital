# ✈️ Capital 

Repositório destinado ao primeiro exercício-programa da disciplina **PMR3304 - Sistemas de Informação** da Poli-USP para criação de um site estático através da *framework* Hugo.

# 📝 Tarefas 

Requisitos e restrições da implementação:

✔️ Não usar temas do Hugo

✔️ Ter pelo menos os três leiautes principais (`index`, `single` e `list`) definidos na pasta `layouts`

✔️ Em todos os leiautes devem ser aplicadas as *tags* estruturais do HTML5, pelo menos `<header>`, `<main>` e `<footer>` e, quando aplicável, usar `<section>` e `<article>` para dividir o conteúdo principal

✔️ Possuir uma barra de navegação dentro de uma *tag* `<nav>`

✔️ O estilo deve ser aplicado através de um arquivo CSS, possivelmente gerado com SASS

✔️ Em pelo menos uma página, deve-se usar o Flexbox ou Grid para controlar o posicionamento de elementos

✔️ Os leiautes devem ser responsivos, devendo apresentar pelo menos uma regra de *media query* em cada página, possivelmente afetando o Flexbox/Grid e/ou o menu de navegação

✔️ Deve incluir pelo menos um subdiretório de conteúdo com, no mínimo, três páginas individuais
