# Capital
## Guia de viagem
||||||||||
{{< figure src="/images/land.jpg" alt="landpage-pic" >}}
||||||||||
{{< feature "/images/beach-icon.png" "Pontos Turísticos" >}} 
    Conheça os pontos turísticos de antemão e saiba o que vale a pena visitar!
{{< /feature >}}

{{< feature "/images/pamphlet-icon.png" "Dicas" >}} 
    Tudo que você precisa saber antes de fazer aquela tão sonhada viagem!
{{< /feature >}}

{{< feature "/images/ticket-icon.png" "Atrações" >}} 
    Veja o que tem de especial pra aproveitar em cada lugar, eventos e atrações únicos!
{{< /feature >}}