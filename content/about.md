---
title: "Sobre"
date: 2021-10-14T15:12:12-03:00
draft: false
---

![about-pic](/images/about.jpg)

||||||||||

## Sobre
O **Capital** é um guia rápido para te ajudar a descobrir o próximo destino da sua viagem! Aqui você encontra um resumo rápido dos pontos turísticos imperdíveis de cada lugar e ainda confere os eventos que vão coincidir com a aventura na qual você quer embarcar! Tudo isso acompanhado das informações mais importantes para saber de antemão e aproveitar ainda mais a sua viagem, chega de contratempos!
