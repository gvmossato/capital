---
title: "Londres"
date: 2021-10-14T15:25:38-03:00
draft: false
---

![city-pic](/images/london.jpg)

||||||||||

Londres é a capital do Reino Unido e também sua maior metrópole. Polo econômico e uma das mais antigas cidades grandes do mundo!

||||||||||

{{< tip "Não fique parado no meio da escada rolante!" >}}
    Assim como em outras grandes metrópoles do mundo as pessoas tem pressa, então, se você quiser ficar parado na escada rolante, deixe a esquerda livre para quem quer ir andando.
{{< /tip >}}

{{< tip "Teatro ou cinema? Deixar pra última hora pode ser uma boa ideia!" >}}
    Não é raro os cinemas e teatros oferecerem descontos quando você tentar comprar ingressos no dia da apresentação, só tome cuidado para não estar esgotado!
{{< /tip >}}

{{< tip "Não pense duas vezes antes de usar os ônibus!" >}}
    Londres é famosa pelos ônibus de dois andares, então não exite em usá-los! Na verdade, utilizar outros meios de transporte privado pode acabar saindo bem mais caro e demorado.
{{< /tip >}}

{{< tip "\"Um chá, por favor\" não faz sentido nenhum!" >}}
    As tradicionais casas de chá servem uma enormidade de variedades de chás, então olhe o cardápio antes e  seja específico!
{{< /tip >}}

{{< tip "Cuidado com a conta do restaurante!" >}}
    Tradicionalmente a "caixinha" pelo serviço do garçom é de 10% a 15% e pode já estar inclusa na conta, então fique atento!
{{< /tip >}}

||||||||||

* **Big Ben:** o Palácio de Westminster (as casas do parlamento) e a Torre Elizabeth (que abriga o sino Big Ben), estão entre os pontos turísticos mais icônicos de Londres, que são deslumbrantes de dia e contam com uma iluminação espetacular de noite!

* **Palácio de Buckingham:** trata-se da casa e do "escritório" da rainha, um dos únicos locais de trabalho da realeza que ainda existem até hoje. Durante o verão, visistantes podem conhecer 19 salas do palácio, decoradas com tesouros e pinturas da própria realeza.

* **London Eye:** aproveite uma incrível vista 360° de Londres em uma roda gigante de 135 metros de altura. Observe lá de cima os pontos turísticos mais famosos da cidade, incluindo até mesmo o Palácio de Buckingham e o Palácio de Westminster.

||||||||||

* **Hogwarts:** aproveite sua passagem por Londres para conhecer um pouco mais sobre o universo de Harry Potter com um tour único pelos estúdios da Warner Bros.

* **Natal:** se for viajar no final de ano, não deixe de aproveitar o tour noturno das luzes de natal, um passeio de ônibus guiado para apreciar toda a decoração natalina de Londres.
