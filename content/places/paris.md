---
title: "Paris"
date: 2021-10-14T17:18:43-03:00
draft: false
---

![city-pic](/images/paris.jpg)

||||||||||

Capital da França e também sua cidade mais populosa, um dos principais centros europeus de gastronomia e arte!

||||||||||

{{< tip "Não fique parado no meio da escada rolante!" >}}
    Assim como em outras grandes metrópoles do mundo as pessoas tem pressa, então, se você quiser ficar parado na escada rolante, deixe a esquerda livre para quem quer ir andando.
{{< /tip >}}

{{< tip "É sempre bom saber o básico do idioma!" >}}
    Cumprimentos e agradecimentos, do "bom dia" ao "obrigado" são sempre úteis e podem vencer barreiras iniciais que algumas pessoas tem com turistas.
{{< /tip >}}

{{< tip "As coisas mudam em Agosto!" >}}
    Como em alguns outros países da Europa, no mês de agosto boa parte dos franceses viaja para outras regiões e com isso o movimento diminui bastante, então espere uma cidade mais tranquila e estabelecimentos fechados.
{{< /tip >}}

{{< tip "Pão, queijo e vinho!" >}}
    Não pense duas vezes antes de experimentar as iguarias locais, inclusive, passar em uma lojinha especializada em queijos é uma ótima pedida.
{{< /tip >}}

||||||||||

* **Torre Eiffel:** um dos pontos turísticos mais conhecidos do mundo, a Torre Eiffel é tão bela quanto os cartões postais mostram, com seus 324 metros de altura é uma visita imperdível para quem vai à Paris.

* **Arco do Triunfo:** tenha um ponto de vista único e deslumbrante de Paris a partir do topo de um terraço paranorâmico carregado de majestosidade e história. Com 50 metros de altura é um monumento de saltar os olhos. 

||||||||||

* **Museu do Louvre:** enquanto estiver em Paris, aproveite para agendar uma visita guiada ao museu do Louvre, será uma oportunidade única ver de perto a Venus de Milo e a Mona Lisa!

* **Catacumbas:** um passeio que pode não agradar a todos os gostos, mas que com certeza será único. Visite o lado mais escuro da Cidade das Luzes em um *tour* guiada pelo complexo sistema de túneis subterrâneos de Paris.
