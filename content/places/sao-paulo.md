---
title: "São Paulo"
date: 2021-10-14T15:25:44-03:00
draft: false
---

![city-pic](/images/sao-paulo.jpg)

||||||||||

Uma das maiores cidades do hemisfério sul, com clima tropical e tudo que uma metrópole pode oferecer!

||||||||||

{{< tip "Não fique parado no meio da escada rolante!" >}}
    Assim como em outras grandes metrópoles do mundo as pessoas tem pressa, então, se você quiser ficar parado na escada rolante, deixe a direita livre para quem quer ir andando.
{{< /tip >}}

{{< tip "Cuidado com o transporte público!" >}}
    As experiências podem variar muito: passear de metrô pelos pontos turísticos aos fins de semana é uma ótima ideia, mas cuidado com o horário de pico!
{{< /tip >}}

{{< tip "Desfrute da vida urbana!" >}}
    A cidade oferece a cada esquina uma forma de arte e culinária própria, então esteja atento à comida de rua e aos grafites!
{{< /tip >}}

||||||||||

* **Parque Ibirapuera:** um excelente lugar para se fazer um passeio natural em meio à vida urbana. Um piquinique ou treino a céu aberto tão são ótimos programas para os dias ensolarados, mas não é só isso: ainda dá pra visitar o Planetário e o Museu de Arte Moderna.

* **MASP:** o Museu de Arte de São Paulo é sem dúvidas uma ótima escolha para quem aprecia píturas e esculturas, ou está interessado em explorar mais desse universo. O acervo conta com obras de Van Gogh, Manet, Debret, Picasso, Portinari e muitos outros!

* **Avenida Paulista:** uma excelente amostra da vida urbana, a avenida é a casa de muitas lojas, cafeterias, restaurantes, shoppings e galerias de arte, incluindo o MASP. E não para por aí, ela também recebe inúmeros eventos ao longo do ano e costuma ter o acesso de veículos restringido em alguns dias, então é possível aproveitar tudo com muito mais calma.

||||||||||

* **Carnaval:** a festa de rua mais conhecida do Brasil também encontra sua forma em São Paulo, se estiver indo conhecer a cidade no início do ano, prepare-se para muita festa e música.

* **Virada Cultural:** um festival de arte de 24 horas com apresentações musicais para todos os gostos em diversos palcos, tudo isso gratuito!
